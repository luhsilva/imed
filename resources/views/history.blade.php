@extends('layouts.app_web')
<style type="text/css">
    .content .card {overflow-x: scroll;}
</style>
@section('content')
<div class="main-panel">
        <div class="content">
            <div class="container-fluid">
                @if(isset($history))
                    <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Finalize o atendimento para ter o historico</h4>
                            </div>
                            <div class="content">
                                <form method="POST" action="{{url('/history/update/' . $history['id'])}}">
                                    @csrf
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Atendimento:</label>
                                                <select class="form-control" id="type_of_service" name="type_of_service" style="border-radius: 5px; border-color: black" required >
                                                    @if(isset($history['type_of_service']))
                                                        <option value="{{$history['type_of_service']}}">{{$history['type_of_service']}}</option>
                                                    @else
                                                        <option value="">Selecione</option>
                                                    @endif
                                                    <option value="UBS/Posto de saúde/Centro de saúde">UBS/Posto de saúde/Centro de saúde</option>
                                                    <option value="AMA">AMA</option>
                                                    <option value="UPA">UPA</option>
                                                    <option value="SUS/Pronto Socorro">SUS/Pronto Socorro</option>
                                                    <option value="vacinacao">Vacinação</option>
                                                    <option value="Unidade Especializadas">Unidade Especializadas</option>
                                                    <option value="Saúde Mental">Saúde Mental</option>
                                                    <option value="Unidade DST/AIDS">Unidade DST/AIDS</option>
                                                    <option value="Unidade Emergência">Unidade Emergência</option>
                                                    <option value="Hospital">Hospital</option>
                                                    <option value="Vigilância em saúde">Vigilância em saúde</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Endereço:</label>
                                                <input type="text" class="form-control border-input" placeholder="Endereço" value="{{isset($history['address']) ? $history['address'] : ''}}" name="address" required>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Sintomas:</label>
                                                <input type="text" class="form-control border-input" placeholder="Sintomas" value="{{isset($history['symptoms']) ? $history['symptoms'] : ''}}" name="symptoms">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Você finalizou o atendimento?</label>
                                                <select class="form-control" id="open" name="open" style="border-radius: 5px; border-color: black" required >
                                                    @if(isset($history['open']))
                                                        <option value="{{$history['open']}}">{{$history['open'] == 1 ? 'Aberto' : 'Fechado'}}</option>
                                                        @if($history['open'] == 0)
                                                            <option value="1">Aberto</option>
                                                        @else
                                                            <option value="0">Fechado</option>
                                                        @endif
                                                    @else
                                                        <option value="">Selecione</option>
                                                        <option value="1">Aberto</option>
                                                        <option value="0">Fechado</option>
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="">Você compareceu a esse endereço? </label><br>
                                                <select class="form-control" id="went" name="went" style="border-radius: 5px; border-color: black" required >
                                                    @if(isset($history['went']))
                                                        <option value="{{$history['went']}}">{{$history['went'] == 0 ? 'Não' : 'Sim' }}</option>

                                                        @if($history['went'] == 0)
                                                            <option value="1">Sim</option>
                                                        @else
                                                            <option value="0">Não</option>
                                                        @endif
                                                    @else
                                                        <option value="">Selecione</option>
                                                        <option value="1">Sim</option>
                                                        <option value="0">Não</option>
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="">Comentário</label><br>
                                                <input type="text" class="form-control border-input" placeholder="Comentario" value="{{isset($history['comment']) ? $history['comment'] : ''}}" name="comment">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="text-center">
                                    <button type="submit" class="btn btn-info btn-fill btn-wd">Salvar <i class="ti-save"></i></button>
                                    </div>
                                    <div class="clearfix"></div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Historico de atendimento</h4>
                            </div>
                            <div class="content table-responsive table-full-width">
                                <table class="table table-striped">
                                    <thead>
                                        <th>Atendimento</th>
                                        <th>Endereço</th>
                                        <th>Sintomas</th>
                                        <!-- <th>Processo</th> -->
                                        <th>Compareceu?</th>
                                        <th>Comentario</th>
                                        <th>Data</th>
                                        <th>Ação</th>
                                    </thead>
                                    <tbody>
                                        @foreach($histories as $history)
                                            <form method="GET" action="{{url('/history/edit/'. $history['id'])}}">
                                                <tr>
                                                    <td>{{$history['type_of_service']}}</td>
                                                    <td>{{$history['address']}}</td>
                                                    <td>{{$history['symptoms']}}</td>
                                                    <!-- <td>{{$history['open'] == 1 ? 'Aberto' : 'Fechado'}}</td> -->
                                                    <td>{{$history['went'] == 0 ? 'Não' : 'Sim' }}</td>
                                                    <td>{{$history['comment']}}</td>
                                                    <td>{{$history['created_at']}}</td>
                                                    <td><button type="submit" class="btn btn-fill btn-primary">Editar</button></td>
                                                </tr>
                                            </form>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>
@endsection
