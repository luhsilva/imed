@extends('layouts.app_web')

@section('content')
@if(isset($error))
<div class="main-panel">
    <div class="alert alert-danger" role="alert">
        <h4 class="alert-heading">Por favor ative a localização</h4>
        <p>Para ter acesso ao conteúdo dessa página é necessário que você ative a sua localização
        </p>
        <hr>
    </div>
</div>
@else
<div class="main-panel">
   <div class="maps">
       <div>
       @if (isset($filter['error']))
            <div class="alert alert-danger">
                <ul>
                    <li>{{ $filter['error'] }}</li>
                </ul>
            </div>
            @foreach($hospitais as $key => $hospital)
                @foreach($hospital as $address)
                    <ul class="hidden">
                        <li>
                            <span>{{ $address['lat'] }},{{ $address['lng']}},{{$key }} </span><br>
                        </li>
                    </ul>
                @endforeach
            @endforeach

        @elseif(!empty($filter))
            @foreach($filter as $key => $hospital)
                @foreach($hospital as $address)
                    <ul class="hidden">
                        <li>
                            <span>{{ $address['lat'] }},{{ $address['lng']}},{{$key }} </span><br>
                        </li>
                    </ul>
                @endforeach
            @endforeach

        @elseif(!empty($hospitais))
            @foreach($hospitais as $key => $hospital)
                @foreach($hospital as $address)
                    <ul class="hidden">
                        <li>
                            <span>{{ $address['lat'] }},{{ $address['lng']}},{{$key }} </span><br>
                        </li>
                    </ul>
                @endforeach
            @endforeach
        @endif
       </div>
       <div class="container-fluid">
           <div class="row mobile-hide">
                <div class="col-lg-3 col-sm-6 ">
                    <div class="card">
                        <div class="content">
                            <div class="row">
                                <div class="col-xs-5">
                                    <div class="icon-big icon-warning text-center">
                                        <a href=""><p>UBS</p></a>
                                    </div>
                                </div>
                                <div class="col-xs-7">
                                    <div class="numbers">
                                        {{count($hospitais['ubs'])}}
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <hr />
                                <div class="stats">
                                    <p>Próximo de você</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="card">
                        <div class="content">
                            <div class="row">
                                <div class="col-xs-5">
                                    <div class="icon-big icon-success text-center">
                                        <a href=""><p>AMA</p></a>
                                    </div>
                                </div>
                                <div class="col-xs-7">
                                    <div class="numbers">
                                        {{count($hospitais['ama'])}}
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <hr />
                                <div class="stats">
                                    <p>Próximo de você</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="card">
                        <div class="content">
                            <div class="row">
                                <div class="col-xs-5">
                                    <div class="icon-big icon-danger text-center">
                                        <a href=""><p>UPA</p></a>
                                    </div>
                                </div>
                                <div class="col-xs-7">
                                    <div class="numbers">
                                        {{count($hospitais['upa'])}}
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <hr />
                                <div class="stats">
                                    <p>Próximo de você</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="card">
                        <div class="content">
                            <div class="row">
                                <div class="col-xs-5">
                                    <div class="icon-big icon-info text-center">
                                        <a href=""><p>Hospitais</p></a>
                                    </div>
                                </div>
                                <div class="col-xs-7">
                                    <div class="numbers">
                                        {{count($hospitais['sus'])}}
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <hr />
                                <div class="stats">
                                    <p>Próximo de você</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

           <div class="row">
                <div class="col-md-6">
                   <div class="card">
                       <div class="header">
                           <h4 class="title">Atendimento</h4>
                       </div>
                       <div class="content" style="height: 104px">
                        <form method="GET" action="{{route('home')}}">
                           <div class="col-md-12">
                               <div class="radio-busca">
                                   <select class="form-control border-input" id="atendimento" name="atendimentoTipo" required>
                                   @if(isset($request['atendimentoTipo'])) 
                                        <option value="{{$request['atendimentoTipo']}}">Emergência</option>
                                        <option value="Endereco">Endereço</option>
                                        <option value="Especialidade">Especialidade</option>
                                        <option value="Tipo">Tipo</option>
                                        <option value="Consulta">Consulta marcada</option>                                        
                                   @elseif(isset($request['type']))
                                        @if($request['type'] == 'Especialidade') 
                                            <option value="{{$request['type']}}">{{$request['type']}}</option>
                                            <option value="Endereco">Endereço</option>
                                            <option value="Emergencia">Emergência</option>
                                            <option value="Tipo">Tipo</option>
                                            <option value="Consulta">Consulta marcada</option>
                                        @elseif($request['type'] == 'Tipo') 
                                            <option value="{{$request['type']}}">{{$request['type']}}</option>
                                            <option value="Endereco">Endereço</option>
                                            <option value="Emergencia">Emergência</option>
                                            <option value="Especialidade">Especialidade</option>
                                            <option value="Consulta">Consulta marcada</option>
                                        @elseif($request['type'] == 'Consulta') 
                                            <option value="{{$request['type']}}">{{$request['type']}}</option>
                                            <option value="Endereco">Endereço</option>
                                            <option value="Emergencia">Emergência</option>
                                            <option value="Especialidade">Especialidade</option>
                                            <option value="Tipo">Tipo</option>
                                        @elseif($request['type'] == 'Endereco') 
                                            <option value="{{$request['type']}}">Endereço</option>
                                            <option value="Emergencia">Emergência</option>
                                            <option value="Especialidade">Especialidade</option>
                                            <option value="Tipo">Tipo</option>
                                            <option value="Consulta">Consulta marcada</option>
                                        @endif
                                    @else 
                                        <option value="">Selecione</option> 
                                        <option value="Endereco">Endereço</option>
                                        <option value="Emergencia">Emergência</option>
                                        <option value="Especialidade">Especialidade</option>
                                        <option value="Tipo">Tipo</option>
                                        <option value="Consulta">Consulta marcada</option>
                                   @endif
                                   </select>
                               </div>
                           </div>
                        </form>
                        </div>
                   </div>
               </div>

                <div class="col-md-6" id="search">
                    <div class="card">
                        <div class="header">
                            <h4 class="title title-filter">Qual endereço?</h4>
                        </div>
                        <div class="content search-all" style="height: 104px;">
                            <form method="GET" action="{{route('home')}}">
                                <div class="col-md-10 col-sm-10 col-lg-10" >
                                    <input id="type" class="form-control hide" type="text" name="type" value="Endereco">
                                    @if(isset($request['Search']))
                                        <input id="pac-input" class="form-control autocomplete border-input" type="text" placeholder="Search" aria-label="Search" name="Search" value="{{$request['Search']}}">
                                    @else
                                        <input id="pac-input" class="form-control autocomplete border-input" type="text" placeholder="Search" aria-label="Search" name="Search" value="">
                                    @endif
                                    <select class="form-control border-input" name="atendimento" id="type-hospital" required>
                                        @if(isset($request['atendimento']))
                                            @if($request['atendimento'] == 'sus')
                                                <option value="{{$request['atendimento']}}">Hospital/SUS</option>
                                                <option value="ama">AMA</option>
                                                <option value="upa">UPA</option>
                                                <option value="ubs">UBS</option>

                                            @elseif($request['atendimento'] == 'ama')
                                                <option value="{{$request['atendimento']}}">AMA</option>    
                                                <option value="sus">Hospital/SUS</option>
                                                <option value="upa">UPA</option>
                                                <option value="ubs">UBS</option>

                                            @elseif($request['atendimento'] == 'upa')
                                                <option value="{{$request['atendimento']}}">UPA</option>
                                                <option value="sus">Hospital/SUS</option>
                                                <option value="ama">AMA</option>                                                
                                                <option value="ubs">UBS</option>

                                            @elseif($request['atendimento'] == 'ubs')
                                                <option value="{{$request['atendimento']}}">UBS</option>
                                                <option value="sus">Hospital/SUS</option>
                                                <option value="ama">AMA</option>
                                                <option value="upa">UPA</option>
                                            @endif
                                        @else 
                                            <option value="sus">Selecione</option>
                                            <option value="sus">Hospital/SUS</option>
                                            <option value="ama">AMA</option>
                                            <option value="upa">UPA</option>
                                            <option value="ubs">UBS</option>
                                        @endif
                                    </select>
                                </div>
                                <div class="col-md-2 col-sm-2 col-lg-2 search-button" >
                                    <button type="submit" class="btn"><i class="ti-search"></i></button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
   </div>
    <div class="col-md-6 col-sm-6 col-lg-6" style="margin-bottom: 5px; position: sticky">
        <div class="card">
            <div class="header">
                <h4 class="title">Posto de emegência mais próximo</h4>
            </div>
            <div id="floating-panel">
                <div id="output">
                    @if(!empty($filter) && !isset($filter['error']))
                        @foreach($filter as $hospital)
                            @foreach($hospital as $address)
                                <ul>
                                    <li>
                                        <span>Nome: {{$address['name']}}</span><br>
                                        <span>Endereço: {{$address['address']}}</span><br>
                                        <a target="_blank" href="https://maps.google.com/?saddr=Current+Location&daddr={{$address['name'] . " " . $address['address']}}"><span class="hospital" data-address="{{$address['address']}}" data-name="{{$address['name']}}">Mostrar no mapa</span></a><a style="color:#333;text-decoration:none;"> - </a>
                                        <a style="color:red;" href="tel:192">Ligar para o SAMU</a><br>
                                    </li>
                                </ul>
                            @endforeach
                        @endforeach
                    @elseif(!empty($hospitais))
                        @foreach($hospitais as $hospital)
                            @foreach($hospital as $address)
                                <ul>
                                    <li>
                                        <span>Nome: {{$address['name']}}</span><br>
                                        <span >Endereço: {{$address['address']}}</span><br>
                                        <a target="_blank" href="https://maps.google.com/?saddr=Current+Location&daddr={{$address['name'] . " " . $address['address']}}"><span class="hospital" data-address="{{$address['address']}}" data-name="{{$address['name']}}">Mostrar no mapa</span></a><a style="color:#333;text-decoration:none;"> - </a>
                                        <a style="color:red;" href="tel:192">Ligar para o SAMU</a><br>
                                    </li>
                                </ul>
                            @endforeach
                        @endforeach
                    @else
                        <div class="alert alert-danger">No results found or Google API is busy!</div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="maps col-md-6 col-sm-6 col-lg-6" id="map"></div>
</div>
@endif
@endsection

