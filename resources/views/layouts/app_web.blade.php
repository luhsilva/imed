<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="apple-touch-icon" sizes="76x76" href="{{asset('img/apple-icon.png')}}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{asset('img/favicon.png')}}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <title>iMed</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />

    <!-- Bootstrap core CSS     -->
    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="{{asset('css/animate.min.css')}}" rel="stylesheet"/>

    <!--  Paper Dashboard core CSS    -->
    <link href="{{asset('css/paper-dashboard.css')}}" rel="stylesheet"/>
    <link href="{{asset('css/style.css')}}" rel="stylesheet"/>

    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="{{asset('css/demo.css')}}" rel="stylesheet" />

    <!--  Fonts and icons     -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>
    <link href="{{asset('css/themify-icons.css')}}" rel="stylesheet">

</head>
<body>

<div class="">
    <div class="sidebar" data-background-color="white" data-active-color="danger">
        <div class="sidebar-wrapper" style="overflow-x: hidden;">
            <div class="logo">
                <div class="img-login" style="">
                    <img src="{{asset('image/iMED_frame.png')}}" alt="" style="width: 46%">
                </div>
            </div>

            <ul class="nav nav-font">
                <li class="@if(isset($active[0])) active @endif Dashboard">
                    <a href="{{route('home')}}">
                        <i class="ti-panel"></i>
                        <p>Dashboard</p>
                    </a>
                </li>
                <li class="@if(isset($active[1])) active @endif Perfil">
                    <a href="{{route('user')}}">
                        <i class="ti-user"></i>
                        <p>Prontuário</p>
                    </a>
                </li>
                <li class="@if(isset($active[2])) active @endif Historico">
                    <a href="{{route('history')}}">
                        <i class="ti-view-list-alt"></i>
                        <p>Histórico</p>
                    </a>
                </li>
                <li class="@if(isset($active[3])) active @endif Consultas">
                    <a href="{{route('consultation')}}">
                        <i class="ti-pencil-alt2"></i>
                        <p>Consultas marcadas</p>
                    </a>
                </li>
                <li class="@if(isset($active[4])) active @endif Notificações">
                    <a href="{{route('notifications')}}">
                        <i class="ti-bell"></i>
                        <p>Notificações</p>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>

<div class="container">
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar bar1"></span>
                    <span class="icon-bar bar2"></span>
                    <span class="icon-bar bar3"></span>
                </button>
            </div>
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="ti-bell"></i>
                            <p class="notification"></p>
                            <p>Notificações</p>
                            <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu menu_notification">
                        </ul>
                    </li>
                    <li>
                        <a href="{{route('logout')}}">
                            <p>Sair</p>
                        </a>
                    </li>
                </ul>

            </div>
        </div>
    </nav>
    @yield('content')
</div>

</body>

<!--   Core JS Files   -->
<script src="{{asset('js/jquery.min.js')}}" type="text/javascript"></script>
<script src="{{asset('js/bootstrap.min.js')}}" type="text/javascript"></script>

<!--  Checkbox, Radio & Switch Plugins -->
<script src="{{asset('js/bootstrap-checkbox-radio.js')}}"></script>

<!--  Charts Plugin -->
<script src="{{asset('js/chartist.min.js')}}"></script>
<script src="{{asset('js/maps.js')}}"></script>

<!--  Notifications Plugin    -->
<script src="{{asset('js/bootstrap-notify.js')}}"></script>

<!--  Google Maps Plugin    -->
{{--chave paga--}}
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBJsWCjna5XcwIwpvLaOZlH0ibsMV8NIOc&callback=init&libraries=places"></script>
<!-- <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCuXP_cDyEUcCkyc3gzzlZsBrt1ztSQ-c4&callback=init&libraries=places"></script> -->


<!-- Paper Dashboard Core javascript and methods for Demo purpose -->
<script src="{{asset('js/paper-dashboard.js')}}"></script>
<script src="{{asset('js/imed.js')}}"></script>

<!-- Paper Dashboard DEMO methods, don't include it in your project! -->
<script src="{{asset('js/demo.js')}}"></script>

<!-- Consulta CEP -->
<script src="{{asset('js/consultaCEP.js')}}" ></script>
</html>
