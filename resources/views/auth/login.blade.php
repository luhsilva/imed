@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row login-tela">
        <form class="form-signin mg-btm" method="POST" action="{{ route('login') }}">
            {{--<h3 class="heading-desc title-imed">iMed</h3>--}}
            <div class="img-login" style="">
                <img src="{{asset('image/iMED_frame.png')}}" alt="" style="width: 46%">
            </div>
            <div class="social-box">
                <div class="row mg-btm">
                    <div class="col-md-12">
                        <button onclick="window.location='{{ url('/redirect') }}'" type="button" class="loginBtn loginBtn--google">
                            Login com Gmail
                        </button>
                    </div>
                </div>
            </div>
            <div class="main">
                <div class="card">
                    <div class="card-body">
                        <form method="POST" action="{{ route('login') }}">
                            @csrf
                            <div class="form-group row">
                                <label for="email" class="col-sm-3 col-form-label text-md-right">{{ __('E-Mail') }}</label>
                                <div class="col-md-9">
                                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>
                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="password" class="col-md-3 col-form-label text-md-right">{{ __('Senha') }}</label>
                                <div class="col-md-9">
                                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                    <div class="form-group row">
                                        <div class="col-md-6 offset-md-4">
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                                <label class="form-check-label" for="remember">
                                                    {{ __('Remember') }}
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row mb-0 button-login">
                                <div class="col-xs-6 col-md-6 pull-right">
                                    <button type="submit" class="btn btn-large btn-danger pull-right">Login</button>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    {{ __('Esqueci minha senha') }}
                                </a>
                                <a class="btn btn-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                <span class="clearfix"></span>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
