@extends('layouts.app_web')
<style type="text/css">
       .content .card {overflow-x: scroll;}
</style>
@section('content')
    <div class="main-panel">
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Preencha aqui as consultas agendadas</h4>
                            </div>
                            <div class="content">
                                <form method="POST" action="{{route('consultationStore')}}" enctype="multipart/form-data">
                                    @csrf
                                    @if ($errors->any())
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Endereço:</label>
                                                <input type="text" class="form-control border-input" placeholder="Endereço" value="" name="address" required>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Atendimento:</label>
                                                <select class="form-control border-input" id="atendimento" name="type_of_service" required >
                                                    <option value="UBS/Posto de saúde/Centro de saúde">UBS/Posto de saúde/Centro de saúde</option>
                                                    <option value="AMA">AMA</option>
                                                    <option value="UPA">UPA</option>
                                                    <option value="SUS/Pronto Socorro">SUS/Pronto Socorro</option>
                                                    <option value="vacinacao">Vacinação</option>
                                                    <option value="Unidade Especializadas">Unidade Especializadas</option>
                                                    <option value="Saúde Mental">Saúde Mental</option>
                                                    <option value="Unidade DST/AIDS">Unidade DST/AIDS</option>
                                                    <option value="Unidade Emergência">Unidade Emergência</option>
                                                    <option value="Hospital">Hospital</option>
                                                    <option value="Vigilância em saúde">Vigilância em saúde</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Data da consulta:</label>
                                                <input type="date" class="form-control border-input" placeholder="Data da consulta" value="" name="date_medical_records" required>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Meio de transporte:</label>
                                                {{--<input type="text" class="form-control border-input" placeholder="Meio de transporte" value="" name="" required>--}}
                                                <select class="form-control border-input" id="transport" name="means_of_transport" required>
                                                    <option value="">Selecione</option>
                                                    <option value="Carro">Carro</option>
                                                    <option value="Transporte publico">Transporte público</option>
                                                    <option value="A pé">A pé</option>
                                                    <option value="Bicicleta">Bicicleta</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Especialidade</label>
                                                <input type="text" class="form-control border-input border-input" value="" id="specialties" name="specialties" require>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="">Upload da guia </label><br>
                                                <div class="btn col-md-12 form-control border-input">
                                                <input type="file" class="file" name="upload_medical_name">
                                                    upload de imagem <i class="ti-upload"></i>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="">Descrição</label><br>
                                            <input type="text"  class="form-control border-input" name="descriptions">
                                        </div>
                                    </div>

                                    <div class="text-center">
                                        <button type="submit" class="btn btn-info btn-fill btn-wd">Salvar <i class="ti-save"></i></button>
                                    </div>
                                    <div class="clearfix"></div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Histórico de consultas</h4>
                            </div>
                            <div class="content table-responsive table-full-width">
                            <div class="content table-responsive table-full-width">
                                <table class="table table-striped">
                                    <thead>
                                    <th>Atendimento</th>
                                    <th>Endereço</th>
                                    <th>Meio de transporte:</th>
                                    <th>Data da consulta:</th>
                                    <th>Especialidade</th>
                                    <th>Descrição</th>
                                    <th>Guia</th>
                                    <th>Ação</th>
                                    </thead>
                                    <tbody>
                                    @foreach($consultations as $consultation)
                                        <form method="GET" action="{{url('/consultation/delet/'. $consultation['id'])}}">
                                            <tr>
                                                <td>{{$consultation['type_of_service']}}</td>
                                                <td>{{$consultation['address']}}</td>
                                                <td>{{$consultation['means_of_transport']}}</td>
                                                <td>{{date('d/m/Y', strtotime($consultation['date_medical_records']))}}</td>
                                                <td>{{$consultation['specialty']['name']}}</td>
                                                <td>{{$consultation['descriptions']}}</td>
                                                <td>
                                                @if(!empty($consultation['upload_medical_name']))
                                                    <a target="blank" href="{{ asset('storage/guias/' .Auth::user()->id.'/'.$consultation->upload_medical_name) }}">
                                                        <img 
                                                            style="max-width: 100%;" 
                                                            src="{{ asset('storage/guias/'.Auth::user()->id.'/'.$consultation->upload_medical_name) }}"
                                                        >
                                                    </a>
                                                @endif
                                                </td>
                                                <td><button type="submit" class="btn btn-fill btn-danger">Delete</button></td>
                                            </tr>
                                        </form>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
