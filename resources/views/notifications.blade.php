@extends('layouts.app_web')

@section('content')
<div class="main-panel">
<div class="content">
    <div class="container-fluid">
        <div class="card">
            <div class="header">
                <h4 class="title">Agenda de suas próximas consultas</h4>
            </div>
            <div class="content">
                <div class="row">
                    <div class="col-md-6">
                        <h5>Agenda de suas próximas consultas</h5>
                            @if(isset($notifications))
                                @foreach($notifications as $notification)
                                <div class="alert alert-success">
                                    <span><b> Data: {{date('d/m/Y', strtotime($notification['date_medical_records']))}} </b><br>
                                    <b> Atendimento: {{$notification['type_of_service']}} </b><br>
                                    <b> Endereço: {{$notification['address']}} </b><br>
                                    <b> Especialidade: {{$notification['specialty']['name']}} </b><br>
                                        @if(!empty($notification['descriptions']))
                                            <b> Descrição: {{$notification['descriptions']}} </b><br>
                                        @endif
                                        @if(!empty($notification['upload_medical_name']))
                                            <b> Guia: <a target="blank" href="{{ asset('storage/guias/' .Auth::user()->id.'/'.$notification->upload_medical_name) }}">
                                                Arquivo</a> </b><br>
                                        @endif
                                    </span>
                                </div>
                                @endforeach
                            @endif
                    </div>
                    <div class="col-md-6">
                        <h5>Consultas de HOJE e AMANHÃ</h5>
                        @if(isset($consultations))
                            @foreach($consultations as $consultation)
                            <div class="alert alert-warning">
                                <span><b> Data: {{date('d/m/Y', strtotime($consultation['date_medical_records']))}} </b><br>
                                    <b> Atendimento: {{$consultation['type_of_service']}} </b><br>
                                    <b> Endereço: {{$consultation['address']}} </b><br>
                                    <b> Especialidade: {{$consultation['specialty']['name']}} </b><br>
                                    @if(!empty($consultation['descriptions']))
                                        <b> Descrição: {{$consultation['descriptions']}} </b><br>
                                    @endif
                                    @if(!empty($consultation['upload_medical_name']))
                                        <b> Guia: <a target="blank" href="{{ asset('storage/guias/' .Auth::user()->id.'/'.$consultation->upload_medical_name) }}">
                                                Arquivo</a> </b><br>
                                    @endif
                                </span>
                            </div>
                            @endforeach
                        @endif
                    </div>
                </div>
                <br>
                <br>
            </div>
        </div>
    </div>
</div>
@endsection
