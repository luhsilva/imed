@extends('layouts.app_web')
<style type="text/css">
    .content form fieldset legend{
        margin-bottom: 15px;
        padding-bottom: 5px;
        border-bottom: 1px solid #948f87;
    }
</style>
@section('content')
    <div class="main-panel">
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class="card">
                            <div class="header">
                                @if($save)
                                    <div class="alert alert-success" role="alert">
                                        Salvo com sucesso
                                    </div>
                                @endif
                                @if($user->first)
                                    <h4 class="title">Finalize seu perfil</h4>
                                @else
                                    <!-- <h4 class="title">Edite</h4> -->
                                @endif
                            </div>
                            <div class="content">
                                <form method="POST" action="{{route('configUser')}}">
                                    @csrf
                                    <fieldset>
                                        <legend>Informações pessoais:</legend>
                                        <div class="row">
                                            <div class="col-md-9">
                                                <div class="form-group">
                                                    <label>Nome Completo:</label>
                                                    <input type="text" class="form-control border-input" placeholder="Name" value="{{$user->name}}" name="name" required>
                                                </div>
                                            </div>
                                            
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label>Sexo:</label>
                                                    <select class="form-control border-input" id="sex" name="sex" required>
                                                        <option value="Female">Feminino</option>
                                                        <option value="Male">Masculino</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Data de Nascimento:</label>
                                                    <input type="date" class="form-control border-input" placeholder="nascimento" value="{{date('Y-m-d', strtotime($user['userMedicalRecord']['birthday']))}}" name="birthday" required>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="state">Estado de nascimento:</label>
                                                    <select class="form-control border-input" id="state_id" name="state_of_nature_id" required>
                                                        @if (!empty($user['userMedicalRecord']['stateNature']['name']))
                                                            <option value="{{$user['userMedicalRecord']['stateNature']['id']}}">{{$user['userMedicalRecord']['stateNature']['name']}}</option>
                                                        @else
                                                            <option value="">Selecione um estado</option>
                                                        @endif
                                                            @foreach($states as $state)
                                                                <option value="{{$state->id}}">{{$state->name}}</option>
                                                            @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="city">Cidade de nascimento:</label>
                                                    <select class="form-control border-input" id="city_id" name="city_naturalness_id" required>
                                                        @if (!empty($user['userMedicalRecord']['cityNaturalness']['name']))
                                                            <option value="{{$user['userMedicalRecord']['cityNaturalness']['id']}}">{{$user['userMedicalRecord']['cityNaturalness']['name']}}</option>
                                                            @foreach($cities as $city)
                                                                <option value="{{$city->id}}">{{$city->name}}</option>
                                                            @endforeach
                                                        @else
                                                            <option class="citySelect">Selecione uma cidade</option>
                                                        @endif
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Nome da mãe:</label>
                                                    <input type="text" class="form-control border-input" placeholder="Nome da mãe" value="{{$user['userMedicalRecord']['complete_name_mother']}}" name="complete_name_mother" required>
                                                </div>
                                            </div>
                                        </div>
                                    <fieldset>
                                        <legend>Contato:</legend>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>E-mail:</label>
                                                    <input type="email" class="form-control border-input" placeholder="E-mail" value="{{$user->email}}" name="email" required>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Celular:</label>
                                                    <input type="text" name="cell_phone" class="form-control border-input" placeholder="Celular" value="{{$user['userAddress']['cell_phone']}}" required>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Telefone fixo:</label>
                                                    <input type="text" class="form-control border-input" placeholder="Telefone fixo" value="{{$user['userAddress']['cell_phone']}}" name="phone">
                                                </div>
                                            </div>                                        
                                        </div>
                                    </fieldset>
                                    </fieldset>
                                    <br>
                                    <fieldset>
                                        <legend>Endereço:</legend>
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label>CEP:</label>
                                                    <input id="cep" type="text" class="form-control border-input" placeholder="CEP" name="zip_code" value="{{$user['userAddress']['zip_code']}}" required>
                                                </div>
                                            </div>
                                            <div class="col-md-7">
                                                <div class="form-group">
                                                    <label>Endereço:</label>
                                                    <input id="endereco" type="text" name="address" class="form-control border-input" placeholder="Endereço" value="{{$user['userAddress']['address']}}" required>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label>Nº</label>
                                                    <input type="text" class="form-control border-input" name="address_number" placeholder="Nº" value="{{$user['userAddress']['address_number']}}" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Complemento:</label>
                                                    <input type="text" class="form-control border-input" name="complement" placeholder="Complemento" value="{{$user['userAddress']['complement']}}">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Bairro:</label>
                                                    <input id="bairro" type="text" name="neighborhood" class="form-control border-input" placeholder="Bairro" value="{{$user['userAddress']['neighborhood']}}" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="state_id">Estado:</label>
                                                    <select id="uf" class="form-control border-input" name="state_id" required>
                                                        @if (!empty($user['userAddress']['state']['name']))
                                                            <option value="{{$user['userAddress']['state']['id']}}">{{$user['userAddress']['state']['name']}}</option>
                                                        @else
                                                            <option value="">Selecione um estado</option>
                                                        @endif
                                                            @foreach($states as $state)
                                                                <option value="{{$state->id}}">{{$state->name}}</option>
                                                            @endforeach
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="city_id">Cidade:</label>
                                                    <select id="cidade" class="form-control border-input" name="city_id" required>
                                                        @if (!empty($user['userAddress']['city']['name']))
                                                            <option value="{{$user['userAddress']['city']['id']}}">{{$user['userAddress']['city']['name']}}</option>
                                                            @foreach($cities as $city)
                                                                <option value="{{$city->id}}">{{$city->name}}</option>
                                                            @endforeach
                                                        @else
                                                            <option class="citySelect">Selecione uma cidade</option>
                                                        @endif
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                    <div class="text-center">
                                        <button type="submit" class="btn btn-info btn-fill btn-wd">Salvar <i class="ti-save"></i></button>
                                    </div>
                                    <div class="clearfix"></div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
