<?php

namespace App;

use App\Models\UserAddress;
use App\Models\UserMedicalRecord;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'google_id',
        'email',
        'password',
        'first',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function userAddress()
    {
        return $this->hasOne(UserAddress::class, 'user_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\hasOne
     */
    public function userMedicalRecord()
    {
        return $this->hasOne(UserMedicalRecord::class, 'user_id', 'id');
    }
}
