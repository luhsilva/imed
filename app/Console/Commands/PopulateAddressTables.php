<?php

namespace App\Console\Commands;

use App\Http\Controllers\Populate\PopulateCitiesController;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class PopulateAddressTables extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'populate:tablesAddress';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->populateStates();
        new PopulateCitiesController();
    }

    protected function populateStates()
    {
        echo 'Populando as Tabelas';

        DB::table('states')->insert([
            "name" => "Rondônia",
            "abbr" => "RO",
        ]);

        DB::table('states')->insert([
            "name" => "Acre",
            "abbr" => "AC",
        ]);

        DB::table('states')->insert([
            "name" => "Amazonas",
            "abbr" => "AM",
        ]);


        DB::table('states')->insert([
            "name" => "Roraima",
            "abbr" => "RR",
        ]);

        DB::table('states')->insert([
            "name" => "Pará",
            "abbr" => "PA",
        ]);

        DB::table('states')->insert([
            "name" => "Amapá",
            "abbr" => "AP",
        ]);

        echo 'aguarde ...';

        DB::table('states')->insert([
            "name" => "Tocantins",
            "abbr" => "TO",
        ]);

        DB::table('states')->insert([
            "name" => "Maranhão",
            "abbr" => "MA",
        ]);

        DB::table('states')->insert([
            "name" => "Piauí",
            "abbr" => "PI",
        ]);

        echo 'espera mais um pouquinho ...';

        DB::table('states')->insert([
            "name" => "Ceará",
            "abbr" => "CE",
        ]);

        DB::table('states')->insert([
            "name" => "Rio Grande do Norte",
            "abbr" => "RN",
        ]);

        DB::table('states')->insert([
            "name" => "Paraíba",
            "abbr" => "PB",
        ]);

        DB::table('states')->insert([
            "name" => "Pernambuco",
            "abbr" => "PE",
        ]);

        DB::table('states')->insert([
            "name" => "Alagoas",
            "abbr" => "AL",
        ]);

        DB::table('states')->insert([
            "name" => "Sergipe",
            "abbr" => "SE",
        ]);

        echo 'Estamos quase terminando ...';

        DB::table('states')->insert([
            "name" => "Bahia",
            "abbr" => "BA",
        ]);

        DB::table('states')->insert([
            "name" => "Minas Gerais",
            "abbr" => "MG",
        ]);

        DB::table('states')->insert([
            "name" => "Espírito Santo",
            "abbr" => "ES",
        ]);

        DB::table('states')->insert([
            "name" => "Rio de Janeiro",
            "abbr" => "RJ",
        ]);

        DB::table('states')->insert([
            "name" => "São Paulo",
            "abbr" => "SP",
        ]);
        DB::table('states')->insert([
            "name" => "Paraná",
            "abbr" => "PR",
        ]);

        echo 'O brasil tem muitos estados ...';

        DB::table('states')->insert([
            "name" => "Santa Catarina",
            "abbr" => "SC",
        ]);

        DB::table('states')->insert([
            "name" => "Rio Grande do Sul",
            "abbr" => "RS",
        ]);

        DB::table('states')->insert([
            "name" => "Mato Grosso do Sul",
            "abbr" => "MS",
        ]);

        DB::table('states')->insert([
            "name" => "Mato Grosso",
            "abbr" => "MT",
        ]);

        DB::table('states')->insert([
            "name" => "Goiás",
            "abbr" => "GO",
        ]);

        DB::table('states')->insert([
            "name" => "Distrito Federal",
            "abbr" => "DF",
        ]);

        echo 'Terminamos os estados, agora vamos para as cidades...';
    }
}
