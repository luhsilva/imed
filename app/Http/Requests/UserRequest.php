<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name.required' => 'required',
            'email.required' => 'required',
            'password.required' => 'required',
            'birthday.required' => 'required',
            'sex.required' => 'required',
            'complete_name_mother.required' => 'required',
            'state_of_nature_id.required' => 'required',
            'municipality_naturalness_id.required' => 'required',
            'neighborhood_id.required' => 'required',
            'municipalitie_id.required' => 'required',
            'state_id.required' => 'required',
            'city_id.required' => 'required',
            'address.required' => 'required',
            'address_number.required' => 'required',
            'zip_code.required' => 'required',
            'cell_phone.required' => 'required',
            'phone.required' => 'required',
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'Name is required',
            'email.required' => 'email is required',
            'password.required' => 'password is required',
            'birthday.required' => 'birthday is required',
            'sex.required' => 'sex is required',
            'complete_name_mother.required' => 'complete_name_mother is required',
            'state_of_nature_id.required' => 'state_of_nature_id is required',
            'municipality_naturalness_id.required' => 'municipality_naturalness_id is required',
            'neighborhood_id.required' => 'neighborhood_id is required',
            'municipalitie_id.required' => 'municipalitie_id is required',
            'state_id.required' => 'state_id is required',
            'city_id.required' => 'city_id is required',
            'address.required' => 'address is required',
            'address_number.required' => 'address_number is required',
            'zip_code.required' => 'zip_code is required',
            'cell_phone.required' => 'cell_phone is required',
            'phone.required' => 'phone is required'
        ];
    }
}
