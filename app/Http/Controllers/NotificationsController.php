<?php

namespace App\Http\Controllers;

use App\Models\MedicalConsultation;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class NotificationsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::where('id', Auth::id())->first();

        if ($user->first) return redirect('/user');

        $date  = date('Y-m-d');
        $date2 = date('Y-m-d', strtotime("+2 days",strtotime($date)));

        $notification = MedicalConsultation::where('user_id', Auth::id())
            ->whereDate('date_medical_records', ">=", $date2)->orderBy('date_medical_records', 'ASC')->get();

        $notification->load('specialty');

        $consultations = $this->getNotification();

        // active menu
        $data['active'][4] = true;

        return view('notifications', $data)->with([
            'notifications' => $notification,
            'consultations' => $consultations
        ]);
    }

    public function getNotification()
    {
        $date = date('Y-m-d');
        $date2 = date('Y-m-d', strtotime("+1 days",strtotime($date)));

        return MedicalConsultation::where('user_id', Auth::id())
            ->whereBetween('date_medical_records', [$date, $date2])
            ->orderBy('date_medical_records', 'ASC')
            ->get();
    }
}
