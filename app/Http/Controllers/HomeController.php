<?php

namespace App\Http\Controllers;

use App\Models\specialty;
use App\Models\Symptoms;
use App\User;
use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;


class HomeController extends Controller
{
    protected $maps;
    protected $userController;

    /**
     * HomeController constructor.
     * @param UserController $user
     */
    public function __construct(
        UserController $user,
        MapsController $maps
    ){
        $this->middleware('auth');
        $this->userController = $user;
        $this->maps = $maps;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, HistoryController $history)
    {
        $filter = [];

        if (in_array($request['type'], ['Consulta', 'Endereco'])) {
            $validator = $this->validateSearch($request);

            if ($validator->fails()) {
                $filter['error'] = 'Preencha o campo de endereço';
            } else {
                return $this->getAddress($request, $history);
            }
        }
        
        
        $user = User::where('id', Auth::id())->first();

        if ($user->first) return redirect('/user');

        if (!isset($_COOKIE['latitude'])) return view('home')->with(['error' => 'Ative a localização']);
        // "error" => "Preencha o campo de especialidade"
        if (! Cache::has('hospital')) Cache::put('hospital', $this->getHostpitais(), 240);
        // Cache::put('hospital', $this->getHostpitais(), 240);

        $hospitais = Cache::get('hospital');
    
        if ( ( isset($request['type']) || isset($request['atendimentoTipo'])) 
                && (!in_array($request['type'], ['Consulta', 'Endereco'])) ) 
                $filter = $this->getFilter($request, $history);

        $data['active'][0] = true;

        return view('home', $data)
            ->with([
                'user' => $user,
                'hospitais' => $hospitais,
                'filter' => $filter,
                'request' => $request->all()
            ]);
    }

    /**
     * Verifica quantos hospitais tem proximo a sua localização
     */
    public function getHostpitais()
    {
        $sus = $this->maps->getHospitals($_COOKIE['latitude'], $_COOKIE['longitude'],'SUS');
        $ubs = $this->maps->getHospitals($_COOKIE['latitude'], $_COOKIE['longitude'],'UBS');
        $ama = $this->maps->getHospitals($_COOKIE['latitude'], $_COOKIE['longitude'],'AMA');
        $upa = $this->maps->getHospitals($_COOKIE['latitude'], $_COOKIE['longitude'],'UPA');

        $hospital['sus'] = $sus;
        $hospital['ubs'] = $ubs;
        $hospital['ama'] = $ama;
        $hospital['upa'] = $upa;

        return $hospital;
    }

    public function getFilter($request, $history)
    {
        $hospitais = Cache::get('hospital');

        if ($request['type'] == 'tipo') {
            return $this->getType($request, $hospitais);
        }

        if ($request['atendimentoTipo'] == 'Emergencia') {
            return $this->getEmergencia($request, $hospitais);
        }

        if ($request['type'] == 'Especialidade') {
            return $this->getSpecialty($request, $hospitais);   
        }
    }

    public function getType($request, $hospitais)
    {
        $validator =  Validator::make($request->all(), [
            'type' => 'required',
            'atendimentoTipo' => 'required',
        ]);
        
        if ($validator->fails()) {
            return ['error' => 'Escolha o atendimento que esta procurando'];
        }
        
        $hospital[$request['atendimentoTipo']] = $hospitais[$request['atendimentoTipo']];
        
        return $hospital;
    }

    public function getAddress($request, $history) 
    {

        $data['address'] = $request['Search'];
        $history->insertHistory($data);

        return Redirect::to('https://maps.google.com/?saddr=Current+Location&daddr=' . $request['Search']);
    }

    public function getEmergencia($request, $hospitais)
    {
        $hospital['sus'] = $hospitais['sus'];
        $hospital['upa'] = $hospitais['upa'];

        return $hospital;
    }

    public function getSpecialty($request, $hospitais)
    {
        $validator = $this->validateSearch($request);

        if ($validator->fails()) {
            return ['error' => 'Preencha o campo de especialidade'];
        }

        $specialty = specialty::where('name', $request['Search'])->first();

        if (!empty($specialty)) {
            
            if ($specialty['type_of_service'] == 'AME') {
                $specialty['type_of_service'] = 'AMA';
            }

            $hospital[strtolower($specialty['type_of_service'])] = $hospitais[strtolower($specialty['type_of_service'])];
            return $hospital;
        } 
          
        return ['error' => 'Especialidade ' . $request['specialties'] . ' não existe'];
    }

    public function validateSearch($request)
    {
        return  Validator::make($request->all(), [
            'type' => 'required',
            'Search' => 'required',
        ]);
    }
}
