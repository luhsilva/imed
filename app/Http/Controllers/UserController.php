<?php

namespace App\Http\Controllers;

use App\Models\UserAddress;
use App\Models\UserMedicalRecord;
use App\Repository\UserRepository;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    /**
     * @var StatesController
     */
    protected $states;

    /**
     * @var
     */
    protected $repository;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(
        StatesController $states,
        UserRepository $repository
    ) {
        $this->middleware('auth');
        $this->states     = $states;
        $this->repository = $repository;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($save = 0)
    {
        $cities = '';

        $user = User::find(Auth::id())
                ->load(
                    'userAddress',
                    'userAddress.state',
                    'userAddress.city',
                    'userMedicalRecord',
                    'userMedicalRecord.stateNature',
                    'userMedicalRecord.cityNaturalness'
                );

        if(!empty($user['userAddress']['state'])) {
            $city = new CityController;
            $cities = $city->getCities($user['userAddress']['state']['id']);
        }

        // active menu
        $data['active'][1] = true;

        return view('user', $data)
            ->with([
                'user' => $user,
                'states' => $this->states->index(),
                'cities' => $cities,
                'save' => $save
            ]);
    }



    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->repository->updateOrCreateUser($request->all());
        $this->repository->updateOrCreateUserMedicalRecord($request->all());
        $this->repository->updateOrCreateUserAddress($request->all());

        return $this->index(1);
    }
}
