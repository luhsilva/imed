<?php

namespace App\Http\Controllers;

class MapsController extends Controller
{
    //
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('maps');
    }

    public function getAddress($latitude, $longitude, $address)
    {

    }

    /**
     * @param $lat
     * @param $lon
     * @param $referral_type
     * @return array|string
     */
    public function getHospitals($lat, $lon, $referral_type)
    {
        /* Get raw json data from Google API and decode it */
        $rawJSON    = file_get_contents("https://maps.googleapis.com/maps/api/place/textsearch/json?location=$lat,$lon&radius=500&query=$referral_type&type=health&key=AIzaSyDQVxgs29yvqcEHrdV7okAdJU60lkYO6Oc");
        $decodeJSON = json_decode($rawJSON, TRUE);
            
        /* Google API busy or too many queries for today. */
        if($decodeJSON['status'] == "OVER_QUERY_LIMIT"){
            return ["OVER_QUERY_LIMIT"];
        }

        $results    = array();      // Return array
        $i          = 0;            // Index for return array
        $found      = false;        // Bool for checking if the place found is a hospital or medical center

        /* Loop through the array */
        foreach ($decodeJSON['results'] as $result) {
            /* Loop through place types */
            foreach ($result['types'] as $ptype) {
                if ("health" == $ptype) {
                    $found = true;
                    break;
                } else {
                    $found = false;
                }
            }
            if ($found == false) {
                continue;
            }

            $results[$i]['name']        = $result['name'];
            $results[$i]['address']     = $result['formatted_address'];
            $results[$i]['distance']   = $this->distance($lat, $lon, $result['geometry']['location']['lat'], $result['geometry']['location']['lng'], "K");
            $results[$i]['lat']   = $result['geometry']['location']['lat'];
            $results[$i]['lng']   = $result['geometry']['location']['lng'];

            $i++;
        }

        /* Sort results by distance */
        usort($results, array("App\Http\Controllers\MapsController", "compareDistance"));
        return $results;
    }

    /* Helper function for calculating distance between two coordinates */
    static function distance($lat1, $lon1, $lat2, $lon2, $unit) {

        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit = strtoupper($unit);

        if ($unit == "K") {
            return ($miles * 1.609344);
        } else if ($unit == "N") {
            return ($miles * 0.8684);
        } else {
            return $miles;
        }
    }

    /* Comparison function for usort */
    public function compareDistance($a, $b){
        return $a["distance"] - $b["distance"];
    }
}
