<?php

namespace App\Http\Controllers;

use App\Models\History;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HistoryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::where('id', Auth::id())->first();

        if ($user->first) return redirect('/user');

        $history = History::where('user_id', Auth::id())->get();

        // active menu
        $data['active'][2] = true;

        return view('history', $data)
            ->with(['histories' => $history]);
    }

    /**
     * @param Request $request
     */
    public function store(Request $request)
    {
        $data['type_of_service'] = $request['name'];
        $data['address'] = $request['address'];

        $this->insertHistory($data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Request $request, int $id)
    {
        $histories = History::where('user_id', Auth::id())->get();

        $history = History::where('id', $id)->first();

        return view('history')
            ->with([
                'history' => $history,
                'histories' => $histories
            ]);
    }

    public function update(Request $request, int $id)
    {
        $data = $request->all();
        unset($data['_token']);

        History::where('id', $id)->update($data);

        $histories = History::where('user_id', Auth::id())->get();

        return view('history')
            ->with([
                'histories' => $histories
            ]);
    }

    public function delete($id)
    {
        History::where('id', $id)->delete();
    }

    public function insertHistory($data)
    {
        $data['user_id'] = Auth::id();
        History::create($data);
    }
}