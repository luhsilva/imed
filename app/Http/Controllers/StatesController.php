<?php

namespace App\Http\Controllers;

use App\Models\States;
use Illuminate\Http\Request;

class StatesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return States[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index()
    {
        return States::orderBy('name', 'ASC')->get();
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function getState(Request $request)
    {
        return States::where('abbr', $request['name'])->first();
    }
}
