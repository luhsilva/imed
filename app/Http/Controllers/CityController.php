<?php

namespace App\Http\Controllers;

use App\Models\City;
use Illuminate\Http\Request;

class CityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return $this->getCities($request['state_id']);
    }

    public function getCity(Request $request)
    {
        return City::where('name', $request['name'])->first();
    }

    public function getCities(int $id)
    {
        return City::where('state_id', $id)->orderBy('name', 'ASC')->get();
    }
}
