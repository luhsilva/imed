<?php

namespace App\Http\Controllers;

use App\Models\MedicalConsultation;
use App\Models\specialty;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Storage;

class ConsultationController extends Controller
{
    //
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::where('id', Auth::id())->first();

        if ($user->first) return redirect('/user');

        $consultation = MedicalConsultation::where('user_id', Auth::id())->get();
        $consultation->load('specialty');

        // active menu
        $data['active'][3] = true;

        return view('consultation', $data)
            ->with([
                'consultations' => $consultation,
                'specialties'  => specialty::all()
            ]);
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $specialty_id = specialty::where('name', $request['specialties'])->first();

        if (is_null($specialty_id)) {
            return redirect('consultation')
                    ->withErrors(['specialties' => 'Especialidade ' . $request['specialties'] . ' não existe']);
        }

        $medicalConsultation = $request->all();
        $medicalConsultation['user_id'] = Auth::id();
        $medicalConsultation['specialty_id'] = $specialty_id['id'];
        
        if (!empty($_FILES['upload_medical_name']['name'])) {
            $originalName = $request->upload_medical_name->getClientOriginalName();
            $pathStorage = 'public/guias/' . Auth::id();
            Storage::putFileAs($pathStorage, $request->upload_medical_name, $originalName);
            $medicalConsultation['upload_medical_name'] = $originalName;
        }
        
        unset($medicalConsultation['specialties']);
        MedicalConsultation::create($medicalConsultation);
        return redirect('consultation');
    }

    public function delete(int $id)
    {
        MedicalConsultation::where('id', $id)->delete();
        return redirect('/consultation');
    }
}