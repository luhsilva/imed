<?php

namespace App\Repository;

use App\Models\UserAddress;
use App\Models\UserMedicalRecord;
use App\User;
use Illuminate\Support\Facades\Auth;

class UserRepository
{

    /**
     * @param array $user
     * @return bool
     */
    public function updateOrCreateUser(array $user): bool
    {
        $userCreate['name']  = $user['name'];
        $userCreate['email'] = $user['email'];
        $userCreate['first'] = 0;

        return User::where('id', Auth::id())->update($userCreate);
    }

    /**
     * @param array $user
     * @return bool
     */
    public function updateOrCreateUserMedicalRecord(array $user): bool
    {
        $userCreate['user_id'] = Auth::id();
        $userCreate['birthday'] = \Carbon\Carbon::parse($user['birthday'])->format('Y-m-d');
        $userCreate['sex'] = $user['sex'];
        $userCreate['complete_name_mother'] = $user['complete_name_mother'];
        $userCreate['state_of_nature_id'] = $user['state_of_nature_id'];
        $userCreate['city_naturalness_id'] = $user['city_naturalness_id'] ?? null;

        return $this->updateOrCreateMedicalRecord($userCreate);
    }



    /**
     * @param array $user
     * @return bool
     */
    public function updateOrCreateUserAddress(array $user): bool
    {
        $userCreate['user_id'] = Auth::id();
        $userCreate['neighborhood'] = $user['neighborhood'];
        $userCreate['state_id'] = $user['state_id'];
        $userCreate['city_id'] = $user['city_id'];
        $userCreate['address'] = $user['address'];
        $userCreate['address_number'] = $user['address_number'];
        $userCreate['complement'] = $user['complement'];
        $userCreate['zip_code'] = $user['zip_code'];
        $userCreate['cell_phone'] = $user['cell_phone'];
        $userCreate['phone'] = $user['phone'] ?? null;

        return $this->updateOrCreateAdress($userCreate) ;
    }

    /**
     * @param array $userCreate
     * @return bool
     */
    private function updateOrCreateMedicalRecord(array $userCreate): bool
    {
        $user = UserMedicalRecord::where('user_id', Auth::id())->first();

        if (!empty($user)) {
            return UserMedicalRecord::where('user_id', Auth::id())->update($userCreate);
        }

        UserMedicalRecord::where('user_id', Auth::id())->create($userCreate);
        return true;
    }

    /**
     * @param array $userCreate
     * @return bool
     */
    private function updateOrCreateAdress(array $userCreate) : bool
    {
        $user = UserAddress::where('user_id', Auth::id())->first();

        if (!empty($user)) {
            return UserAddress::where('user_id', Auth::id())->update($userCreate);
        }

        UserAddress::where('user_id', Auth::id())->create($userCreate);
        return true;
    }
}
