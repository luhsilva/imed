<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class UserMedicalRecord extends Model
{
    protected $table =  'user_medical_records';
    /**
     * @var array
     */
    protected $fillable = [
        'user_id',
        'birthday',
        'sex',
        'complete_name_mother',
        'state_of_nature_id',
        'city_naturalness_id',
    ];

    protected $dates = ['birthday'];


    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function stateNature()
    {
        return $this->belongsTo(States::class, 'state_of_nature_id', 'id');
    }

    public function cityNaturalness()
    {
        return $this->belongsTo(City::class, 'city_naturalness_id', 'id');
    }
}
