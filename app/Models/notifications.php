<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class notifications extends Model
{
    //
    protected $fillable = [
        'name',
        'title',
        'description',
        'date_medical_records',
    ];
}
