<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class MedicalConsultation extends Model
{
    protected $table = "appointments_doctors";

    protected $fillable = [
        'user_id',
        'type_of_service',
        'specialty_id',
        'upload_medical_consultations_id',
        'descriptions',
        'address',
        'means_of_transport',
        'date_medical_records',
        'medical_consultation_schedule',
        'upload_medical_name'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function specialty()
    {
        return $this->belongsTo(specialty::class, 'specialty_id', 'id');
    }
}
