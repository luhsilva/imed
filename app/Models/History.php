<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class History extends Model
{
    //
    protected $fillable = [
        'user_id',
        'type_of_service',
        'symptoms',
        'address',
        'open',
        'went',
        'comment',
    ];
}
