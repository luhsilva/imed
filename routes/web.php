<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();
Route::get('/redirect', 'SocialAuthGoogleController@redirect')->name('redirect');
Route::get('/callback', 'SocialAuthGoogleController@callback');
Route::get('/logout', 'SocialAuthGoogleController@logout');

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/symptoms', 'HomeController@symptoms');

Route::get('/filter', 'HomeController@getFilter')->name('filter');

Route::get('/user', 'UserController@index')->name('user');
Route::post('/config/user', 'UserController@store')->name('configUser');

Route::get('/history', 'HistoryController@index')->name('history');
Route::get('/history/store', 'HistoryController@store');
Route::get('/history/edit/{id}', 'HistoryController@edit');
Route::POST('/history/update/{id}', 'HistoryController@update');
Route::POST('/history/delete/{id}', 'HistoryController@delete');
Route::get('/medicalRecord', 'MedicalRecordController@index')->name('medicalRecord');

Route::get('/consultation', 'ConsultationController@index')->name('consultation');
Route::post('/consultation/store', 'ConsultationController@store')->name('consultationStore');
Route::get('/consultation/delet/{id}', 'ConsultationController@delete');

Route::get('/notifications', 'NotificationsController@index')->name('notifications');
Route::get('/notification', 'NotificationsController@getNotification');

Route::get('/cities', 'CityController@index');
Route::get('/city', 'CityController@getCity');

Route::get('/state', 'StatesController@getState');

Route::get('/search/autocomplete', 'SpecialtyController@index');

