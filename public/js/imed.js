$(document).ready(function(){

    $( "#uf" ).change(function() {
        $('#cidade').html("");
        $.ajax({
            url: '/cities',
            type: 'GET',
            data: {
                state_id: $( "#uf" ).val()
            },
            success: function(response)
            {
                $('#cidade').html(
                    response.forEach(function(response) {
                        $("#cidade").append('<option value="' + response.id + '">' + response.name + '</option>')
                    })
                );
            }
        });
    });

    $( "#state_id" ).change(function() {
        $('#city_id').html("");

        $.ajax({
            url: '/cities',
            type: 'GET',
            data: {
                state_id: $( "#state_id" ).val()
            },
            success: function(response)
            {
                console.log(response);
                $('#city_id').html(
                    response.forEach(function(response) {
                        $("#city_id").append('<option value="' + response.id + '">' + response.name + '</option>')
                    })
                );
            }
        });
    });

    $('.radio-busca').change(function () {
        let value = $('#atendimento').val();
        $('#pac-input').val('');
        $('.search-button').show();

        if ('Consulta' == value) {
            $('.title-filter').html('Digite o endereço da sua consulta');
            $('#type').val('Consulta');
            $('#type-hospital').hide();
            $('#pac-input').show();
        }

        if ('Endereco' == value) {
            $('.title-filter').html('Digite o endereço');
            $('#type').val('Endereco');
            $('#type-hospital').hide();
            $('#pac-input').show();
        }

        if ('Emergencia' == value) {
            var form = document.forms[0];
            form.action = "/home";
            form.submit();
            // $('.title-filter').html('O resultado para emergêrcia ja esta aparecendo na sua tela');
            // $('#type').val('Emergencia');
            // $('#type-hospital').hide();
            // $('#pac-input').show();
            // emergencia();
        }

        if ('Tipo' == value) {
            $('.title-filter').html('Qual a rede que esta procurando?');
            $('#type-hospital').show();
            $('#pac-input').hide();
            $('#type').val('Tipo');
        }

        if ('Especialidade' == value) {
            $('.title-filter').html('Qual a especialidade que deseja?');
            $('#type-hospital').hide();
            $('#pac-input').show();
            $('#type').val('Especialidade');
            $('#pac-input').keyup(function () {
                $('.list').remove();
                $('.li-list').remove();
                autocomplete();
            });

        }
    });

    $('.hidden').hide();
    $('.hide').hide();
    let value = $('#atendimento').val();
    if ('Tipo' == value) {
        $('.title-filter').html('Qual a rede que esta procurando?');
        $('#type-hospital').show();
        $('#pac-input').hide();
        $('#type').val('Tipo');
    } else if ('Emergencia' == value) {
        $('.title-filter').html('A busca já foi realizada');
        $('#type-hospital').hide();
        $('#pac-input').hide();
        $('.search-button').hide();
    } else {
        $('#type-hospital').hide();
    }

    //Salva no historico
    $('.hospital').on('click', function(){
        $.ajax({
            url: '/history/store',
            type: 'GET',
            data: {
                address: $(this).data("address"),
                name: $(this).data("name")
            },
        });
    });

    $('#specialties').keyup(function () {
        $('.list-consultation').remove();
        $('.li-list-consultation').remove();
        $.ajax({
            url: '/search/autocomplete',
            type: 'GET',
            data: {
                search: $('#specialties').val()
            },
            success: function(response)
            {
                $('.list-consultation').remove();
                $('.li-list-consultation').remove();
                $('#specialties')
                    .after('<ul class="list-consultation"></ul>')
                response.forEach(function(response) {
                    $('.list-consultation').append("<li class='li-list-consultation'>"+ response.value +"</li><br>")
                });

                $('.li-list-consultation').on('click', function () {
                    var string = $(this).text();
                    $('#specialties').val(string);
                    $('.list-consultation').remove();
                    $('.li-list-consultation').remove();
                })

                if ($('#specialties').val() == '') {
                    $('.list-consultation').remove();
                    $('.li-list-consultation').remove();
                }
            }
        });
    });

    function autocomplete()
    {
        $.ajax({
            url: '/search/autocomplete',
            type: 'GET',
            data: {
                search: $('#pac-input').val()
            },
            success: function(response)
            {
                $('.list').remove();
                $('.li-list').remove();
                $('#pac-input')
                    .after('<ul class="list"></ul>')
                response.forEach(function(response) {
                    $('.list').append("<li class='li-list'>"+ response.value +"</li><br>")
                });

                $('.li-list').on('click', function () {
                    var string = $(this).text();
                    $('#pac-input').val(string);
                    $('#specialties').val(string);
                    $('.list').remove();
                    $('.li-list').remove();
                })

                if ($('#pac-input').val() == '') {
                    $('.list').remove();
                    $('.li-list').remove();
                }
            }
        });
    }

    $('#atendimento').click(function() {
        $('#atendimento').change(function() {
            $('#type').focus();
            $('#pac-input').focus();
            $('#type-hospital').focus();
        });       
    });

    $('#atendimento').click(function() { 
        $('#atendimento').change(function() {
            $('#type').removeClass("blur").addClass("focus"); 
            $('#pac-input').removeClass("blur").addClass("focus"); 
            $('#type-hospital').removeClass("blur").addClass("focus"); 
        });
    });

    $('#atendimento').blur(function() { 
        $('#type').addClass("blur");
        $('#pac-input').addClass("blur");
        $('#type-hospital').addClass("blur");
    });

    $.ajax({
        url: '/notification',
        type: 'GET',
        success: function(response)
        {
            console.log(response.length);
            if (response.length > 0 ) {
                $('.notification').html(response.length)
                $('.notification').html(
                    response.forEach(function(response) {
                        var DateNew = new Date(response.date_medical_records);
                        console.log(DateNew);
                        var day = DateNew.getDate();
                        var month = DateNew.getMonth() + 1;
                        var year = DateNew.getFullYear();

                        $(".menu_notification").append(
                            '<li><a href="/notifications">Consulta dia ' +  day + '/' + month + '/' + year + '</a></li>'
                        )
                    })
                );
            } else {
                $('.dropdown').hide();
            }
        }
    });
});