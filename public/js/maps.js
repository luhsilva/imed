navigator.geolocation.getCurrentPosition(showPostion)

function showPostion(position)
{
    document.cookie = "latitude=" + position.coords.latitude;
    document.cookie = "longitude=" + position.coords.longitude;
    console.log(position)
}

function init()
{
    let lat, lng, map, geocoder, marker;

    const showPos = function (pos) {
        lat = pos.coords.latitude;
        lng = pos.coords.longitude;

        var iconBase = 'https://maps.google.com/mapfiles/kml/shapes/';
        var icons = {
            hospital: {
                icon: "http://maps.google.com/mapfiles/kml/paddle/H.png"
            },
            ama: {
                icon: "http://maps.google.com/mapfiles/kml/paddle/A.png"
            },
            upa: {
                icon: "http://maps.google.com/mapfiles/kml/paddle/U.png"
            },
            ubs: {
                icon: "http://maps.google.com/mapfiles/kml/paddle/B.png"
            },
            eu:{
                icon: "http://maps.google.com/mapfiles/kml/paddle/ylw-stars.png"
            }
        };

        var latlng = new google.maps.LatLng(lat, lng);
        var options = {
            center: latlng,
            zoom: 10,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            styles: [{
                featureType: 'poi',
                stylers: [{ visibility: 'off' }]  // Turn off points of interest.
            }, {
                featureType: 'transit.station',
                stylers: [{ visibility: 'off' }]  // Turn off bus stations, train stations, etc.
            }],
            disableDoubleClickZoom: true
        };

        map = new google.maps.Map(document.getElementById("map"), options);
        geocoder = new google.maps.Geocoder();
        marker = new google.maps.Marker({
            map: map,
            draggable: true,
            icon: icons['eu'].icon,
            title: 'Você',
        });

        var addressesArray = $('.maps ul li').map(function(i, el) {
            return $(el).text();
        }).get();

        for (var x = 0; x < addressesArray.length; x++) {
                let address = addressesArray[x].trim();
                let arrayLatLng = address.split(",");
                let lat = arrayLatLng[0];
                let lng = arrayLatLng[1];
                let latlng = new google.maps.LatLng(lat, lng);

                if (arrayLatLng[2] == 'sus') {
                    var aMarker = new google.maps.Marker({
                        position: latlng,
                        map: map,
                        title: arrayLatLng[2],
                        icon: icons['hospital'].icon,
                    });
                }

                if (arrayLatLng[2] == 'ama') {
                    var aMarker = new google.maps.Marker({
                        position: latlng,
                        map: map,
                        title: arrayLatLng[2],
                        icon: icons['ama'].icon,
                    });
                }

                if (arrayLatLng[2] == 'ubs') {
                    var aMarker = new google.maps.Marker({
                        position: latlng,
                        map: map,
                        title: arrayLatLng[2],
                        icon: icons['ubs'].icon,
                    });
                }

                if (arrayLatLng[2] == 'upa') {
                    var aMarker = new google.maps.Marker({
                        position: latlng,
                        map: map,
                        title: arrayLatLng[2],
                        icon: icons['upa'].icon,
                    });
                }
        }

        marker.setPosition(latlng);
    }

    navigator.geolocation.getCurrentPosition(showPos)
}
