<?php

use App\Console\Commands\PopulateAddressTables;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Database\Migrations\Migration;

class PopulaFildesStatesAndCity extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Artisan::registerCommand(app()
             ->make(PopulateAddressTables::class));
         Artisan::call('populate:tablesAddress');
         Artisan::call('db:seed');
         Artisan::output();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
