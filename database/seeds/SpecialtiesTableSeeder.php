<?php

use Illuminate\Database\Seeder;

class SpecialtiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $seeds[] = [
            'name' => 'Pediatria',
            'type_of_service' => 'UBS',
        ];
        $seeds[] = [
            'name' => 'Ginecologia',
            'type_of_service' => 'UBS',
        ];
        $seeds[] = [
            'name' => 'Clínica Geral',
            'type_of_service' => 'UBS',
        ];
        $seeds[] = [
            'name' => 'Enfermagem',
            'type_of_service' => 'UBS',
        ];
        $seeds[] = [
            'name' => 'Odontologia',
            'type_of_service' => 'UBS',
        ];
        $seeds[] = [
            'name' => 'consultas médicas',
            'type_of_service' => 'UBS',
        ];
        $seeds[] = [
            'name' => 'inalações',
            'type_of_service' => 'UBS',
        ];
        $seeds[] = [
            'name' => 'injeções',
            'type_of_service' => 'UBS',
        ];
        $seeds[] = [
            'name' => 'curativos',
            'type_of_service' => 'UBS',
        ];
        $seeds[] = [
            'name' => 'vacinas',
            'type_of_service' => 'UBS',
        ];
        $seeds[] = [
            'name' => 'coleta de exames laboratoriais',
            'type_of_service' => 'UBS',
        ];
        $seeds[] = [
            'name' => 'medicação básica',
            'type_of_service' => 'UBS',
        ];
        $seeds[] = [
            'name' => 'Radiografia',
            'type_of_service' => 'AMA',
        ];
        $seeds[] = [
            'name' => 'Consultas das clínicas básicas',
            'type_of_service' => 'AMA',
        ];
        $seeds[] = [
            'name' => 'Aferição dos sinais vitais',
            'type_of_service' => 'AMA',
        ];
        $seeds[] = [
            'name' => 'temperatura',
            'type_of_service' => 'AMA',
        ];
        $seeds[] = [
            'name' => 'pressão arterial',
            'type_of_service' => 'AMA',
        ];
        $seeds[] = [
            'name' => 'pulso e respiração',
            'type_of_service' => 'AMA',
        ];
        $seeds[] = [
            'name' => 'hemograma',
            'type_of_service' => 'AMA',
        ];
        $seeds[] = [
            'name' => 'glicemia, amilase',
            'type_of_service' => 'AMA',
        ];
        $seeds[] = [
            'name' => 'uréia',
            'type_of_service' => 'AMA',
        ];
        $seeds[] = [
            'name' => 'creatinina',
            'type_of_service' => 'AMA',
        ];
        $seeds[] = [
            'name' => 'sódio',
            'type_of_service' => 'AMA',
        ];
        $seeds[] = [
            'name' => 'potássio',
            'type_of_service' => 'AMA',
        ];
        $seeds[] = [
            'name' => 'TGO',
            'type_of_service' => 'AMA',
        ];
        $seeds[] = [
            'name' => 'TGP',
            'type_of_service' => 'AMA',
        ];
        $seeds[] = [
            'name' => 'bilirrubinas',
            'type_of_service' => 'AMA',
        ];
        $seeds[] = [
            'name' => 'urina tipo I',
            'type_of_service' => 'AMA',
        ];
        $seeds[] = [
            'name' => 'baciloscopia',
            'type_of_service' => 'AMA',
        ];
        $seeds[] = [
            'name' => 'teste de gravidez',
            'type_of_service' => 'AMA',
        ];
        $seeds[] = [
            'name' => 'Eletrocardiograma',
            'type_of_service' => 'AMA',
        ];
        $seeds[] = [
            'name' => 'Administração de medicamentos orais e injetáveis',
            'type_of_service' => 'AMA',
        ];
        $seeds[] = [
            'name' => 'Inalação',
            'type_of_service' => 'AMA',
        ];
        $seeds[] = [
            'name' => 'Terapia de reidratação oral',
            'type_of_service' => 'AMA',
        ];
        $seeds[] = [
            'name' => 'Hidratação intravenosa',
            'type_of_service' => 'AMA',
        ];
        $seeds[] = [
            'name' => 'Curativo, retirada de pontos',
            'type_of_service' => 'AMA',
        ];
        $seeds[] = [
            'name' => 'Estimular Coleta de Papanicolaou',
            'type_of_service' => 'AMA',
        ];
        $seeds[] = [
            'name' => 'Vacinação',
            'type_of_service' => 'AMA',
        ];
        $seeds[] = [
            'name' => 'cardiologistas/cardiologia',
            'type_of_service' => 'AME',
        ];
        $seeds[] = [
            'name' => 'endocrinologistas',
            'type_of_service' => 'AME',
        ];
        $seeds[] = [
            'name' => 'urologistas',
            'type_of_service' => 'AME',
        ];
        $seeds[] = [
            'name' => 'reumatologistas',
            'type_of_service' => 'AME',
        ];
        $seeds[] = [
            'name' => 'neurologistas',
            'type_of_service' => 'AME',
        ];
        $seeds[] = [
            'name' => 'neurologistas',
            'type_of_service' => 'AME',
        ];
        $seeds[] = [
            'name' => 'angiologistas / vascular',
            'type_of_service' => 'AME',
        ];
        $seeds[] = [
            'name' => 'exames',
            'type_of_service' => 'AME',
        ];
        $seeds[] = [
            'name' => 'pequenas cirurgias',
            'type_of_service' => 'AME',
        ];
        $seeds[] = [
            'name' => 'eletrocardiograma',
            'type_of_service' => 'AME',
        ];
        $seeds[] = [
            'name' => 'teste ergométrico',
            'type_of_service' => 'AME',
        ];
        $seeds[] = [
            'name' => 'holter',
            'type_of_service' => 'AME',
        ];
        $seeds[] = [
            'name' => 'MAPA -  Monitorização Ambulatorial de Pressão Arterial ',
            'type_of_service' => 'AME',
        ];
        $seeds[] = [
            'name' => 'Eletroencefalograma',
            'type_of_service' => 'AME',
        ];
        $seeds[] = [
            'name' => 'Ultrassonografia',
            'type_of_service' => 'AME',
        ];
        $seeds[] = [
            'name' => 'Ecodopplercardiograma',
            'type_of_service' => 'AME',
        ];
        $seeds[] = [
            'name' => 'Doppler Vascular',
            'type_of_service' => 'AME',
        ];
        $seeds[] = [
            'name' => 'Eletroencefalograma',
            'type_of_service' => 'AME',
        ];
        $seeds[] = [
            'name' => 'EXAMES RADIOLÓGICOS',
            'type_of_service' => 'AME',
        ];
        $seeds[] = [
            'name' => 'radiografia',
            'type_of_service' => 'UPA',
        ];
        $seeds[] = [
            'name' => 'eletrocardiografia',
            'type_of_service' => 'UPA',
        ];
        $seeds[] = [
            'name' => 'pediatria',
            'type_of_service' => 'UPA',
        ];
        $seeds[] = [
            'name' => 'laboratório de exames',
            'type_of_service' => 'UPA',
        ];
        $seeds[] = [
            'name' => 'leitos de observação',
            'type_of_service' => 'UPA',
        ];
        $seeds[] = [
            'name' => 'Pressão e febre',
            'type_of_service' => 'UPA',
        ];
        $seeds[] = [
            'name' => 'Fraturas e cortes',
            'type_of_service' => 'UPA',
        ];
        $seeds[] = [
            'name' => 'Infarto e derrame',
            'type_of_service' => 'UPA',
        ];
        $seeds[] = [
            'name' => 'Queda com torsão e muita dor ou suspeita de fratura',
            'type_of_service' => 'UPA',
        ];
        $seeds[] = [
            'name' => 'Cólicas renais',
            'type_of_service' => 'UPA',
        ];
        $seeds[] = [
            'name' => 'falta de ar',
            'type_of_service' => 'UPA',
        ];
        $seeds[] = [
            'name' => 'Convulsão',
            'type_of_service' => 'UPA',
        ];
        $seeds[] = [
            'name' => 'Dores fortes no peito',
            'type_of_service' => 'UPA',
        ];
        $seeds[] = [
            'name' => 'Vômito constante',
            'type_of_service' => 'UPA',
        ];

        foreach ($seeds as $seed) {
            DB::table('specialties')->insert($seed);
        }
    }
}
